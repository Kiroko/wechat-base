// subpkg/login/login.js
import { login } from '../../api/user'
Page({
  /**
   * 页面的初始数据
   */
  data: {
    loginname: '17704051019',
    password: '123456'
  },
  async onLogin() {
    try {
      const res = await login({
        loginname: this.data.loginname,
        password: this.data.password
      })

      if (res.code === 200) {
        wx.reLaunch({
          url: '/pages/home/home',
        })

        // 保存token到本地
        wx.setStorageSync('token', res.data.token)
        wx.setTabBarItem({
          index: 3,
          text: '我的'
        })
      } else {
        wx.showToast({
          title: '用户名或是密码错误!',
          icon: 'error'
        })
      }
    } catch (error) {

    }
  }
})