// subpkg/goods-detail/goods-detail.js
import {request} from '../../utils/request'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',
    goodsDetail:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      id:options.proid
    })
    this.getgoodsDetail()
  },
  async getgoodsDetail(){
    const res = await request({url:`pro/detail/${this.data.id}`})
    this.setData({
      goodsDetail:res.data
    },() => {
      console.log(res.data);
    })
  },
  toCart(){
    wx.switchTab({
      url: '/pages/cart/cart',
    })
  }

})