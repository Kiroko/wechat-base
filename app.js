// app.js
App({
  onLaunch() {
    const token = wx.getStorageSync('token')
    if (!token) {
      wx.setTabBarItem({
        index: 3,
        text: '未登录'
      })
    }
  }
})
