// pages/home/home.js
// 导入请求封装
import { request } from '../../utils/request'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    swipers: [],
    menus: [],
    goodsList: [],
    page: 1,
    pageSize: 10,
    hasmore: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getSwiperData()
    this.getMunusData()
    this.getGoodsListData()
  },
  // 异步获取轮播图数据
  async getSwiperData() {
    const res = await request({ url: 'banner/list' })
    this.setData({
      swipers: res.data
    })
  },
  // 异步获取菜单数据
  async getMunusData() {
    const res = await request({ url: 'menu/list' })
    this.setData({
      menus: res.data
    })
  },
  // 异步获取商品列表数据
  async getGoodsListData() {
    // 定义页面，每次调用函数加一次页面
    const res = await request({
      url: 'pro/list',
      data: {
        // 获取页面数据
        count: this.data.page,
        limitNum: this.data.pageSize
      }
    })
    // 判断当长度不大于0时候则为假，页面的if判断则执行else
    this.setData({
      hasmore: res.data.length > 0
    })
// 渲染商品列表数据
    this.setData({
      goodsList: [...this.data.goodsList, ...res.data]
    })
    wx.stopPullDownRefresh()
  },
  
  // 上拉底部生命钩子，监听拉到底部时候执行操作
  onReachBottom() {
    if (!this.data.hasmore) return
    this.data.page++
    this.getGoodsListData()
  },
  onPullDownRefresh() {
    // 把之前的数据全清空
    this.setData({
      goodsList: [],
    })
    // 重新加载第一页的数据
    this.data.page = 1
    this.getGoodsListData()
  },
  goToDetail(e) {
    wx.navigateTo({
      url:
        '/subpkg/goods-detail/goods-detail?proid=' +e.currentTarget.dataset.proid
    })
  },
  onShow() {
    const token = wx.getStorageSync('token')
    if (!token) {
      wx.setTabBarItem({
        index: 3,
        text: '未登录'
      })
    }
  }
})