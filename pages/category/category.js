// pages/category/category.js
import { request } from '../../utils/request'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    category:[],
    currentIndex:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getCategoryData()
  },
  async getCategoryData(){
    const res = await request({url:'category/list'})
    this.setData({
      category:res.data
    },() => {
      console.log(res.data);
    })
  },
  switchCate(e){
    this.setData({
      currentIndex: e.target.dataset.index
    })
  }
})