// pages/cart/cart.js
Page({
  data: {
    goods: [
      { id: 1, name: "商品1" },
      { id: 2, name: "商品2" },
      { id: 3, name: "商品3" },
    ],
  },
  goToDetail(event) {
    // 跳转到商品详情页
    const { id } = event.currentTarget.dataset;
    wx.navigateTo({
      url: `/pages/detail/detail?id=${id}`,
    });
  },
  onSwipeOpen(event) {
    // 商品卡片侧滑打开回调
    const { index } = event.currentTarget.dataset;
    console.log(`商品${index + 1}被打开`);
  },
  onSwipeClose(event) {
    // 商品卡片侧滑关闭回调
    const { index } = event.currentTarget.dataset;
    console.log(`商品${index + 1}被关闭`);
  },
  deleteItem(event) {
    // 删除商品
    const { index } = event.currentTarget.dataset;
    const { goods } = this.data;
    goods.splice(index, 1);
    this.setData({ goods });
  },
  onchange(){}
});