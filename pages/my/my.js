// pages/my/my.js
import { getUserInfo } from '../../api/user'
import Dialog from '@vant/weapp/dialog/dialog'
Page({
  /**
   * 页面的初始数据
   */
  data: {
    userInfo: null
  },
  onShow() {
    const token = wx.getStorageSync('token')
    if (!token) {
      wx.redirectTo({
        url: '/subpkg/login/login'
      })
    } else {
      // 获取用户信息进行渲染
      this.getUserInfoData()
    }
  },
  async getUserInfoData() {
    const res = await getUserInfo()
    this.setData({
      userInfo: res.data
    }, () => {
      console.log(res.data);
    })
  },
  logout() {
    Dialog.confirm({
      title: '提示',
      message: '确认退出吗?'
    })
      .then(() => {
        wx.removeStorageSync('token')
        wx.redirectTo({
          url: '/subpkg/login/login'
        })
      })
      .catch(() => { }
      )
  }
})