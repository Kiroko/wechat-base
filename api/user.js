import {request} from '../utils/request'

// 封装登录api接口
export const login = data => {
  // 返回示例创建的Promise对象
  return request({
    url: 'user/login',
    method: 'POST',
    data
  })
}

export const getUserInfo = () => {
  return request({
    url: 'user/info'
  })
}