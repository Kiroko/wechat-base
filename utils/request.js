// 封装请求
// api为基地址
const api = 'http://huangjiangjun.top:3001/api/'
// 导出定义的request，传参对象里所需要用到的
export const  request = ({url,method='GET',header={},data={}}) => {
  const token = wx.getStorageSync('token')
  if (token) {
    header.token = token
  }
  // 返回一个异步的promise构造函数，resolve返回正确的数据，reject返回错误信息
  return new Promise((resolve,reject)=>{
    // 微信小程序请求的方法
    wx.request({
      url: `${api}${url}`,
      method,
      header,
      data,
      success:res => {
        resolve(res.data)
      },
      fail:error => {
        reject(error)
      }
    })  
  })
}